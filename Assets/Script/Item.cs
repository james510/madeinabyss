﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject
{
    public int itemCount;
    public int itemID;
    public bool consumable = true;
    public bool equipment = false;
    public int image;
    public int healthRegen;
    public int hungerRegen;
}
