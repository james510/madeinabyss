﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    public int itemID;

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer==8)
        {
            other.GetComponent<PlayerController>().PickupItem(itemID);
            Destroy(gameObject);
        }
    }
}
