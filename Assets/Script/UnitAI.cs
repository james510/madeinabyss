﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAI : MonoBehaviour
{
    public int health = 100;
    public Vector3 velocity;
    Vector3 lastPos;

    void Update ()
    {
        lastPos = transform.position;
	}

    void LateUpdate()
    {
        velocity = (lastPos - transform.position)/Time.deltaTime;
    }
}
