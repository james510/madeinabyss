﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class EnemyAI : MonoBehaviour
{
    UnitAI uai;
    public Vector3 velocity=Vector3.zero;
    RaycastHit hit;
    CharacterController cc;
    public float friction = 0.8f;
    bool isGrounded;
    bool isStunned = false;

	void Awake ()
    {
        cc = GetComponent<CharacterController>();
        uai = GetComponent<UnitAI>();	

	}
	
	void Update ()
    {
        if (isStunned)
            return;
        if(!Physics.Raycast(transform.position,Vector3.down,2f))
            velocity.y += -9.81f;//add check for collision with ground before adding gravity
        velocity *= friction;
        cc.Move(velocity*Time.deltaTime);
	}

    public void TakeDamage(int damage,float stunTime)
    {
        uai.health -= damage;
        Timing.RunCoroutine(Stunlock(stunTime));
    }

    IEnumerator<float> Stunlock(float stunTime)
    {
        isStunned = true;
        yield return Timing.WaitForSeconds(stunTime);
        isStunned = false;
    }
}
