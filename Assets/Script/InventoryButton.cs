﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryButton : MonoBehaviour
{
    public int itemSlot;
    public int itemID=-1;
    public UIController uic;
    public Image image;

	void Start ()
    {
        image = GetComponent<Image>();
        GetComponent<Button>().onClick.AddListener(InventoryClick);
	}
	
    void InventoryClick()
    {
        print(uic.player.GetComponent<PlayerController>().items[itemSlot].itemCount);
        if (itemID==-1)
            return;
        if (uic.itemDict.itemDict[itemID].consumable)
        {
            uic.player.GetComponent<PlayerController>().health += uic.itemDict.itemDict[itemID].healthRegen;
            if (uic.player.GetComponent<PlayerController>().health > uic.player.GetComponent<PlayerController>().maxHealth)
                uic.player.GetComponent<PlayerController>().health = uic.player.GetComponent<PlayerController>().maxHealth;
            uic.player.GetComponent<PlayerController>().hunger += uic.itemDict.itemDict[itemID].hungerRegen;
            if (uic.player.GetComponent<PlayerController>().hunger > uic.player.GetComponent<PlayerController>().maxHunger)
                uic.player.GetComponent<PlayerController>().hunger = uic.player.GetComponent<PlayerController>().maxHunger;
            if(uic.player.GetComponent<PlayerController>().items[itemSlot].itemCount<1)
            {
                uic.player.GetComponent<PlayerController>().items.RemoveAt(itemSlot);
                image.sprite = null;
                itemID = -1;
                itemSlot = -1;

            }
            else
            {
                uic.player.GetComponent<PlayerController>().items[itemSlot].itemCount--;
            }
        }
    }
}
