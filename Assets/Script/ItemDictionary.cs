﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemDictionary : MonoBehaviour
{
    public Dictionary<int,Item> itemDict = new Dictionary<int, Item>();
    public Dictionary<int, Sprite> imageDict = new Dictionary<int, Sprite>();

    public Sprite bandage;
    void Awake ()
    {
        Item clone = ScriptableObject.CreateInstance<Item>();
        clone.itemID = 0;
        clone.consumable = true;
        clone.equipment = false;
        clone.itemCount = 3;
        clone.healthRegen = 10;
        itemDict.Add(0, clone);

        imageDict.Add(0,bandage); 
	}
}
