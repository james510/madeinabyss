﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentButton : MonoBehaviour
{
    public int slotType = 0;
    public int itemID = -1;
    Image buttonImage;
    UIController uic;

    void Start ()
    {
        uic = FindObjectOfType<UIController>();
        buttonImage = GetComponent<Image>();
        GetComponent<Button>().onClick.AddListener(ClickButton);
	}

    void ClickButton()
    {
        if(uic.currentItem)
        {
            if(uic.currentItem.equipment)
            {
                buttonImage.sprite = uic.itemDict.imageDict[uic.currentItem.itemID];
                itemID = uic.currentItem.itemID;
                switch(slotType)
                {
                    default:
                    break;
                }
            }
        }
        else
        {
            if(itemID!=-1)
            {
                buttonImage.sprite = null;
                //uic.player.GetComponent<PlayerController>().items.Add(uic.itemDict.itemDict[itemID].GetComponent<Item>());
            }
        }
    }
}
