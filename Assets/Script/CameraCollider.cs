﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollider : MonoBehaviour
{
    GameObject player;
    GameObject cameraLook;
    Vector3 holdPos;
    RaycastHit hit;
    float currentZoom = 0;

    void Awake()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
        cameraLook = GameObject.Find("CameraLook");
    }

    void Start()
    {
        holdPos = transform.localPosition;
    }

    void Update()
    {
        if(Physics.Raycast(player.transform.position,transform.position-player.transform.position,out hit,10,~(1<<8)))
        {
            transform.position = hit.point;
        }
        else
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, holdPos, Time.deltaTime * 10);
        }
        transform.rotation = Quaternion.LookRotation(cameraLook.transform.position- transform.position);
        currentZoom += Input.GetAxis("Mouse ScrollWheel") / 5;
        currentZoom = Mathf.Clamp(currentZoom, -0.1f, 0.06f);
        transform.position = Vector3.Lerp(transform.position, player.transform.position, currentZoom);
    }
}
