﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;

public class PlayerController : MonoBehaviour
{
    public int health = 100;
    public int hunger = 100;
    public int maxHealth = 100;
    public int maxHunger = 100;

    public float maxSpeed;
    public float walkSpeed;
    public float runSpeed;
    public float friction;
    public float attackCoolDown = 0.1f;

    float maxSpeedNormal;
    float nextAttack = 0;
    float jumpVelocity = 0;
    float gravity = -9.81f;

    bool isGrounded = true;

    CharacterController cc;
    ItemDictionary itemDict;
    UIController uic;

    GameObject mainCamera;
    GameObject hitObject;

    Vector3 velocity;
    Vector3 camForward;
    Vector3 camRight;

    Collider[] hits;

    public List<Item> items = new List<Item>();
    public Item head, body, legs, feet;
    public Item weapon, ammo, pickaxe, ranged;

    void Awake()
    {
        uic = FindObjectOfType<UIController>();
        itemDict = FindObjectOfType<ItemDictionary>();
        cc = GetComponent<CharacterController>();
        mainCamera = Camera.main.gameObject;
        maxSpeedNormal = maxSpeed;
        hitObject = transform.GetChild(0).gameObject;
        hitObject.SetActive(false);
    }

	void Start ()
    {

	}
	
	void Update ()
    {
        if (uic.inventoryBG.activeInHierarchy)
            return;
        camForward = mainCamera.transform.forward;
        camRight = mainCamera.transform.right;
        camForward.y = 0;
        camRight.y = 0;
        camForward.Normalize();
        camRight.Normalize();
        velocity += (camForward * Input.GetAxis("Vertical") + camRight * Input.GetAxis("Horizontal"))*walkSpeed;
        velocity.z=Mathf.Clamp(velocity.z, -maxSpeed, maxSpeed);
        velocity.x=Mathf.Clamp(velocity.x, -maxSpeed, maxSpeed);
        velocity.z *= friction;
        velocity.x *= friction;

        if(Input.GetKey(KeyCode.LeftShift))
        {
            maxSpeed = runSpeed;
        }
        else
        {
            maxSpeed = maxSpeedNormal;
        }
        if(Input.GetKeyDown(KeyCode.Space)&&isGrounded)
        {
            jumpVelocity = 7;
            isGrounded = false;
        }
        if(!isGrounded)
        {
            if (Physics.Raycast(transform.position, -transform.up, 2, ~(1 << 8)))
            {
                isGrounded = true;
            }
        }
        if(Input.GetMouseButtonDown(0) && Time.time > nextAttack)
        {
            hitObject.SetActive(true);
            hits = Physics.OverlapBox(hitObject.transform.position, Vector3.one/2f);
            Timing.RunCoroutine(AttackEffect());
            if (hits.Length>0)
            {
                for(int i=0;i<hits.Length;i++)
                {
                    if (hits[i].GetComponent<EnemyAI>())
                    {
                        hits[i].GetComponent<EnemyAI>().velocity += (hits[i].transform.position - transform.position)*10;
                        if (!weapon)
                            hits[i].GetComponent<UnitAI>().health -= 1;
                    }
                }
            }
            nextAttack = Time.time + attackCoolDown;
        }
        jumpVelocity += gravity * Time.deltaTime;
        velocity.y = jumpVelocity;
        transform.rotation = Quaternion.Euler(0,Mathf.Rad2Deg * Mathf.Atan2(velocity.x, velocity.z), 0);
        cc.Move(velocity * Time.deltaTime);
	}

    IEnumerator<float> AttackEffect()
    {
        yield return Timing.WaitForSeconds(0.1f);
        hitObject.SetActive(false);
    }

    public void PickupItem(int item)
    {
        Item clone = ScriptableObject.CreateInstance<Item>();
        clone.itemID = item;
        clone.equipment = itemDict.itemDict[clone.itemID].equipment;
        clone.consumable = itemDict.itemDict[clone.itemID].consumable;
        clone.image = itemDict.itemDict[clone.itemID].image;
        clone.healthRegen = itemDict.itemDict[clone.itemID].healthRegen;
        clone.hungerRegen = itemDict.itemDict[clone.itemID].hungerRegen;
        clone.itemCount = itemDict.itemDict[clone.itemID].itemCount;
        items.Add(clone);
    }
}
