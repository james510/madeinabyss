﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    GameObject player;
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;
    public float minimumX = -360F;
    public float maximumX = 360F;
    public float minimumY = -60F;
    public float maximumY = 60F;
    public float friction = .95F;
    public float gravity = .1f;
    float rotationY = 1F;
    public float speed;
    public bool canMove=true;

    void Start ()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void LockMouse()
    {
        Cursor.lockState = CursorLockMode.Locked;
        canMove = true;
    }

    public void FreeMouse()
    {
        Cursor.lockState = CursorLockMode.None;
        canMove = false;
    }

    void Update ()
    {
        if(canMove)
        {
            transform.position = Vector3.Slerp(transform.position, player.transform.position, speed * Time.deltaTime);
            if (axes == RotationAxes.MouseXAndY) //Mouse movement translating to body rotation and head rotation
            {
                float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
                rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
                transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
            }
            else if (axes == RotationAxes.MouseX)
            {
                transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
            }
            else
            {
                rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
                rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);
                transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
            }
        }
    }
}
