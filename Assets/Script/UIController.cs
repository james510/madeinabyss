﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public ItemDictionary itemDict;
    public GameObject inventoryButton;

    [HideInInspector]
    public Item currentItem;
    public GameObject mouseFollowObject;
    public GameObject player;
    public GameObject inventoryBG;
    public int inventorySpace;

    List<GameObject> invButtonList = new List<GameObject>();
    GameObject itemBG;
    CameraFollow camFollow;

    void Start ()
    {
        itemDict = FindObjectOfType<ItemDictionary>();
        player = FindObjectOfType<PlayerController>().gameObject;
        inventoryBG = GameObject.Find("InventoryBG");
        itemBG = GameObject.Find("ItemBG");
        camFollow = FindObjectOfType<CameraFollow>();

        CreateButtons();
        inventoryBG.SetActive(false);
	}
	
    public void CreateButtons()
    {
        for(int y=0;y<3;y++)
        {
            for(int x=0;x<5;x++)
            {
                GameObject clone = Instantiate(inventoryButton, itemBG.transform);
                clone.GetComponent<InventoryButton>().uic = this;
                clone.GetComponent<RectTransform>().anchoredPosition = new Vector2(x*210-420, y*-220+220);
                invButtonList.Add(clone);
            }
        }
    }

    public void UpdateInventoryButtons()
    {
        for (int i = 0; i < player.GetComponent<PlayerController>().items.Count;i++)
        {
            invButtonList[i].GetComponent<InventoryButton>().itemID = player.GetComponent<PlayerController>().items[i].itemID;
            invButtonList[i].GetComponent<InventoryButton>().itemSlot = i;
            invButtonList[i].GetComponent<Image>().sprite = itemDict.imageDict[player.GetComponent<PlayerController>().items[i].itemID];
        }
    }

    public void SwapItem(int x, int y)
    {
        int temp = player.GetComponent<PlayerController>().items[x].itemID;
        int temp2 = player.GetComponent<PlayerController>().items[y].itemID;
        player.GetComponent<PlayerController>().items[x].itemID = temp2;
        player.GetComponent<PlayerController>().items[x].equipment = itemDict.itemDict[temp2].equipment;
        player.GetComponent<PlayerController>().items[x].consumable = itemDict.itemDict[temp2].consumable;
        player.GetComponent<PlayerController>().items[x].image = itemDict.itemDict[temp2].image;

        player.GetComponent<PlayerController>().items[y].itemID = temp;
        player.GetComponent<PlayerController>().items[y].equipment = itemDict.itemDict[temp].equipment;
        player.GetComponent<PlayerController>().items[y].consumable = itemDict.itemDict[temp].consumable;
        player.GetComponent<PlayerController>().items[y].image = itemDict.itemDict[temp].image;

        UpdateInventoryButtons();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (!inventoryBG.activeInHierarchy)
            {
                inventoryBG.SetActive(true);
                UpdateInventoryButtons();
                camFollow.FreeMouse();
            }
            else
            {
                inventoryBG.SetActive(false);
                camFollow.LockMouse();
            }
        }
    }
}
